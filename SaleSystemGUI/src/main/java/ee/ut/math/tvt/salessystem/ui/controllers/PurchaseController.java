package ee.ut.math.tvt.salessystem.ui.controllers;

import com.sun.javafx.collections.ObservableListWrapper;
import ee.ut.math.tvt.salessystem.SalesSystemException;
import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import ee.ut.math.tvt.salessystem.logic.ShoppingCart;
import ee.ut.math.tvt.salessystem.ui.InvalidInputException;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

/**
 * Encapsulates everything that has to do with the purchase tab (the tab
 * labelled "Point-of-sale" in the menu). Consists of the purchase menu,
 * current purchase dialog and shopping cart table.
 */
public class PurchaseController implements Initializable {

    private static final Logger log = LogManager.getLogger(PurchaseController.class);

    private final SalesSystemDAO dao;
    private final ShoppingCart shoppingCart;

    @FXML
    private Button newPurchase;
    @FXML
    private Button submitPurchase;
    @FXML
    private Button cancelPurchase;
    @FXML
    private TextField barCodeField;
    @FXML
    private TextField quantityField;
    @FXML
    private ComboBox<String> itemMenu;
    @FXML
    private TextField priceField;
    @FXML
    private Button addItemButton;
    @FXML
    private TableView<SoldItem> purchaseTableView;

    public PurchaseController(SalesSystemDAO dao, ShoppingCart shoppingCart) {
        this.dao = dao;
        this.shoppingCart = shoppingCart;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        cancelPurchase.setDisable(true);
        submitPurchase.setDisable(true);
        purchaseTableView.setItems(new ObservableListWrapper<>(shoppingCart.getAll()));
        disableProductField(true);
        updateDropdown();

        this.itemMenu.valueProperty().addListener((observable, oldValue, newValue) -> {
            fillInputsBySelectedStockItem();
        });
    }

    /** Update the dropdown menu with newest stats from warehouse */
    @FXML
    private void updateDropdown() {
        List<StockItem> allItems = dao.findStockItems();
        if (allItems != null) {
            ObservableList<String> itemList = FXCollections.observableArrayList();
            itemList.addAll(allItems.stream().map(StockItem::getName).collect(Collectors.toList()));
            itemMenu.setItems(itemList);
        }
    }

    /** Event handler for the <code>new purchase</code> event. */
    @FXML
    protected void newPurchaseButtonClicked() {
        log.info("New sale process started");
        try {
            enableInputs();
            updateDropdown();
        } catch (SalesSystemException e) {
            log.error(e.getMessage(), e);
        }
    }

    /**
     * Event handler for the <code>cancel purchase</code> event.
     */
    @FXML
    protected void cancelPurchaseButtonClicked() {
        log.info("Sale cancelled");
        try {
            // Return items to warehouse
            for (SoldItem solditem : shoppingCart.getAll()) {
                StockItem stockitem = dao.findStockItem(solditem.getId());
                stockitem.setQuantity(stockitem.getQuantity() + solditem.getQuantity());
            }
            shoppingCart.cancelCurrentPurchase();
            disableInputs();
            purchaseTableView.refresh();
        } catch (SalesSystemException e) {
            log.error(e.getMessage(), e);
        }
    }

    /**
     * Event handler for the <code>submit purchase</code> event.
     */
    @FXML
    protected void submitPurchaseButtonClicked() {
        log.info("Sale complete");
        try {
            log.info("Contents of the current basket:\n" + shoppingCart.getAll());
            shoppingCart.submitCurrentPurchase();
            disableInputs();
            purchaseTableView.refresh();
        } catch (SalesSystemException e) {
            log.error(e.getMessage(), e);
        }
    }

    // switch UI to the state that allows to proceed with the purchase
    private void enableInputs() {
        resetProductField();
        disableProductField(false);
        cancelPurchase.setDisable(false);
        submitPurchase.setDisable(false);
        newPurchase.setDisable(true);
    }

    // switch UI to the state that allows to initiate new purchase
    private void disableInputs() {
        resetProductField();
        cancelPurchase.setDisable(true);
        submitPurchase.setDisable(true);
        newPurchase.setDisable(false);
        disableProductField(true);
    }

    private void fillInputsBySelectedStockItem() {
        StockItem stockItem = dao.findStockItem(itemMenu.getValue());
        System.out.println(stockItem);
        if (stockItem != null) {
            barCodeField.setText(stockItem.getId().toString());
            priceField.setText(String.valueOf(stockItem.getPrice()));
        } else {
            resetProductField();
        }
    }

    /**
     * Add new item to the cart.
     */
    @FXML
    public void addItemEventHandler() {
        // add chosen item to the shopping cart.
        StockItem stockItem = dao.findStockItem(itemMenu.getValue());
        if (stockItem != null) {
            try {
                int quantity = Integer.parseInt(quantityField.getText());
                if(checkProductQuantity(quantity, stockItem)) {
                    shoppingCart.addItem(new SoldItem(stockItem, quantity));
                    stockItem.setQuantity(stockItem.getQuantity() - quantity);
                }
            } catch (NumberFormatException e) {
                log.error("Attempted to add invalid number of items to cart: " + quantityField.getText());
            } catch (IllegalArgumentException e) {
                log.error("Invalid quantity");
                throw new InvalidInputException("Invalid quantity value or not enough items available.");
            }
            purchaseTableView.refresh();
        }
    }

    /**
     * Checks how many items are available in the warehouse vs attempted to purchase.
     * Returns false if trying to buy more than available.
     */
    private boolean checkProductQuantity(int quantity, StockItem stockItem) {

        if(quantity > stockItem.getQuantity() || quantity <= 0) {

            // Log and display an error to the user
            log.error("Exception: Attempted to add " + quantity + " items to cart, only "
                    + stockItem.getQuantity() + " available.");
            throw new InvalidInputException("Invalid quantity value or not enough items available.");
        }
        return true;
    }

    /**
     * Sets whether or not the product component is enabled.
     */
    private void disableProductField(boolean disable) {
        this.addItemButton.setDisable(disable);
        this.barCodeField.setDisable(disable);
        this.quantityField.setDisable(disable);
        this.priceField.setDisable(disable);
        this.itemMenu.setDisable(disable);
    }

    /**
     * Reset dialog fields.
     */
    private void resetProductField() {
        barCodeField.setText("");
        quantityField.setText("1");
        priceField.setText("");
    }
}
