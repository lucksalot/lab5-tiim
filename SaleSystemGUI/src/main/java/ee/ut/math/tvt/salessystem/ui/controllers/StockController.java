package ee.ut.math.tvt.salessystem.ui.controllers;

import com.sun.javafx.collections.ObservableListWrapper;
import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import ee.ut.math.tvt.salessystem.ui.InvalidInputException;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.net.URL;
import java.util.ResourceBundle;

public class StockController implements Initializable {

    private final SalesSystemDAO dao;
    private static final Logger log = LogManager.getLogger(StockController.class);


    @FXML
    private Button addItem;
    @FXML
    private TableView<StockItem> warehouseTableView;
    @FXML
    private TextField barCodeField;
    @FXML
    private TextField amountField;
    @FXML
    private TextField nameField;
    @FXML
    private TextField priceField;

    public StockController(SalesSystemDAO dao) {
        this.dao = dao;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        refreshStockItems();

        this.barCodeField.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if(!newValue) fillInputsById();
        });
    }

    @FXML
    public void refreshButtonClicked() {
        log.info("warehouse refreshed");
        refreshStockItems();
    }

    private void refreshStockItems() {
        log.info(dao.findStockItems());
        warehouseTableView.setItems(new ObservableListWrapper<>(dao.findStockItems()));
        warehouseTableView.refresh();
    }

    /**
     *  Get ID from barcode, update name and price fields accordingly.
     */
    private void fillInputsById() {
        try {
            int newProductID;
            if ((newProductID = Integer.parseInt(barCodeField.getText())) > 0) {
                dao.findStockItems().stream().filter(inStock -> newProductID == inStock.getId()).forEach(inStock -> {
                    priceField.setText(String.valueOf(inStock.getPrice()));
                    nameField.setText(inStock.getName());
                });
            } else {
                barCodeField.setText(null);
                throw new InvalidInputException("Please insert a valid barcode.");
            }
        }
        catch (NumberFormatException e) {
            throw new InvalidInputException("Please insert a valid barcode.");
        }
    }

    @FXML
    protected void addProductClicked() {
        log.info("Add product button clicked");
        int productBarCode;
        int productAmount;
        double productPrice;
        String productName = nameField.getText();

        try {
            productBarCode = Integer.parseInt(barCodeField.getText());
//            dao.findStockItems().stream().filter(inStock -> inStock.getId() == productBarCode).forEach(inStock -> {
//                priceField.setText(String.valueOf(inStock.getPrice()));
//                nameField.setText(inStock.getName());
//            });
        } catch (NumberFormatException e) {
            log.error("Exception: Attempted to add item to warehouse: \n" +
                    "Invalid bar code: " + barCodeField.getText());
            throw new InvalidInputException("The bar code you entered is not valid.");
        }

        try {
            if((productAmount = Integer.parseInt(amountField.getText())) < 0){
                throw new InvalidInputException("The quantity has to be a positive integer.");
            }
        } catch (NumberFormatException e) {
            log.error("Exception: Attempted to add item to warehouse: \n" +
                    "Invalid amount: " + amountField.getText());
            throw new InvalidInputException("The quantity you entered is not valid.");
        }

        try {
            if((productPrice = Double.parseDouble(priceField.getText())) < 0) {
                throw new InvalidInputException("The price has to be positive.");
            }
        } catch (NumberFormatException e) {
            log.error("Exception: Attempted to add item to warehouse: \n" +
                    "Invalid price: " + priceField.getText());
            throw new InvalidInputException("The price you entered is not valid.");
        }

        //Check if the item is already in the stockItem list
        StockItem stockItem = dao.findStockItem(productBarCode);
        if (stockItem == null) {
            dao.saveStockItem(new StockItem((long) productBarCode, productName, "", productPrice, productAmount));
        } else {
            dao.updateStock(stockItem, productAmount);
        }
        refreshStockItems();
    }
}
