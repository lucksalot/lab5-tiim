package ee.ut.math.tvt.salessystem.ui.controllers;

import javafx.fxml.Initializable;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.net.URL;
import java.util.ResourceBundle;

public class TeamController implements Initializable{

    private static final Logger log = LogManager.getLogger(TeamController.class);

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        log.info("Team controller initialized");
    }
}
