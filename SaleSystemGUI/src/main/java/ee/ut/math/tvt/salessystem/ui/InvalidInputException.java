package ee.ut.math.tvt.salessystem.ui;

import javafx.scene.control.Alert;

public class InvalidInputException extends RuntimeException {

    public InvalidInputException(String message) {
        super(message);

        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Something went wrong.");
        alert.setHeaderText("Invalid value");
        alert.setContentText(message + "\n\nThe value has to be a positive integer." +
                "\nDecimal is allowed in price field.");
        alert.showAndWait();
    }
}
