package ee.ut.math.tvt.salessystem.ui.controllers;

import com.sun.javafx.collections.ObservableListWrapper;
import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.Purchase;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableView;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.net.URL;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

/**
 * Encapsulates everything that has to do with the purchase tab (the tab
 * labelled "History" in the menu).
 */
public class HistoryController implements Initializable {

    private static final Logger log = LogManager.getLogger(HistoryController.class);
    private final SalesSystemDAO dao;

    @FXML
    private Button showBetweenDatesButton;
    @FXML
    private Button showLast10;
    @FXML
    private Button showAll;
    @FXML
    private DatePicker startDatePicker;
    @FXML
    private DatePicker endDatePicker;
    @FXML
    private TableView<Purchase> historyTable;
    @FXML
    private TableView<SoldItem> historyItemsTable;

    public HistoryController(SalesSystemDAO dao) {
        this.dao = dao;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        log.info("History controller initialized");
        historyTable.setItems(new ObservableListWrapper<Purchase>(dao.findAllPurchases()));
        historyTable.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
            if (newSelection != null) {
                Purchase purchase = historyTable.getSelectionModel().getSelectedItem();
                //historyItemsTable.setItems(new ObservableListWrapper<SoldItem>(purchase.getSoldItemList()));
                //log.info(purchase.getSoldItemList());
            }
        });
    }

    @FXML
    protected void showBetweenDatesButtonClicked () {
        try {
            log.info("Show between dates button clicked");
            LocalDate startDate = startDatePicker.getValue();
            LocalDate endDate = endDatePicker.getValue();
            List<Purchase> allPurchases = dao.findAllPurchases();
            List<Purchase> betweenDatesPurchases = new ArrayList<>();
            for (Purchase purchase : allPurchases) {
                LocalDate purchaseDate = purchase.getDateInDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
                if ((purchaseDate.isAfter(startDate) || purchaseDate.isEqual(startDate)) && (purchaseDate.isBefore(endDate) || purchaseDate.isEqual(endDate))) {
                    betweenDatesPurchases.add(purchase);
                }
            }
            historyTable.setItems(new ObservableListWrapper<Purchase>(betweenDatesPurchases));
        } catch (NullPointerException e) {
            log.error("Incorrect date given");
        }
    }


    @FXML
    protected void showLast10Clicked() {
        log.info("Show last 10 purchases button clicked");
        List<Purchase> allPurchases = dao.findAllPurchases();
        if (allPurchases.size() <= 10) {
            historyTable.setItems(new ObservableListWrapper<Purchase>(allPurchases));
        } else {
            historyTable.setItems(new ObservableListWrapper<Purchase>(
                    allPurchases.subList(allPurchases.size()-10, allPurchases.size())));
        }
    }

    @FXML
    protected void showAllClicked() {
        log.info("Show all purchases button clicked");
        log.info(dao.findAllPurchases());
        historyTable.setItems(new ObservableListWrapper<Purchase>(dao.findAllPurchases()));
    }


}
