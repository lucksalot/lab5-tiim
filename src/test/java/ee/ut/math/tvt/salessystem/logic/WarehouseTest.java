package ee.ut.math.tvt.salessystem.logic;

import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by Cardo on 22.11.2016.
 */
public class WarehouseTest {
    private Warehouse warehouse;
    @Before
    public void setUp() throws Exception {
        warehouse = new Warehouse();

    }

    @After
    public void tearDown() throws Exception {

    }

    //testAddingItemBeginsAndCommitsTransaction - check that methods beginTransaction and commitTransaction are both called exactly once and that order
    @Test
    public void testAddingItemBeginsAndCommitsTransaction(){
        Long id = 1L;
        String name = "Free vodka";
        String desc = "";
        Double price = 0.0;
        Integer quantity = 10;
        StockItem item = new StockItem(id,name,desc,price,quantity);
        warehouse.addItem(item);
        Assert.assertTrue(warehouse.getWasTransactionStarted() && warehouse.getWasTransactionFinished());



    }
    //testAddingNewItem - check that a new item is saved through the DAO
    @Test
    public void testAddingNewItem(){
        Long id = 1L;
        String name = "Free vodka";
        String desc = "";
        Double price = 0.0;
        Integer quantity = 10;
        StockItem item = new StockItem(id,name,desc,price,quantity);
        warehouse.addItem(item);
        Assert.assertEquals(warehouse.getItems().get(0),item);


    }
    //testAddingExistingItem - check that adding a new item increases the quantity and the saveStockItem method of the DAO is not called
    @Test
    public void testAddingExistingItem(){
        Long id = 1L;
        String name = "Free vodka";
        String desc = "";
        Double price = 0.0;
        Integer quantity = 10;
        StockItem item = new StockItem(id,name,desc,price,quantity);
        warehouse.addItem(item);
        warehouse.addItem(item);
        Assert.assertTrue(warehouse.findItem(item.getId()).getQuantity() == 20);

    }
    //testAddingItemWithNegativeQuantity - check that adding an item with negative quantity results in an exception
    @Test(expected = IllegalArgumentException.class)
    public void testAddingItemWithNegativeQuantity(){
        Long id = 1L;
        String name = "Free vodka";
        String desc = "";
        Double price = 0.0;
        Integer quantity = -10; // <-----
        StockItem item = new StockItem(id,name,desc,price,quantity);
        warehouse.addItem(item);
    }
}