package ee.ut.math.tvt.salessystem.logic;

import ee.ut.math.tvt.salessystem.dao.InMemorySalesSystemDAO;
import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ShoppingCartAddTest {

    private ShoppingCart shoppingCart;
    private SalesSystemDAO dao;

    @Before
    public void setUp() throws Exception {
        dao = new InMemorySalesSystemDAO();
        shoppingCart = new ShoppingCart(dao);
    }


    // check that adding an existing item increases the quantity
    @Test
    public void testAddingExistingItem() {
        int quantity1 = 5;
        int quantity2 = 9;

        StockItem stockItem = new StockItem((long) 10, "Milk", "", 0.49, 20);
        dao.saveStockItem(stockItem);

        SoldItem soldItem1 = new SoldItem(stockItem, quantity1);
        shoppingCart.addItem(soldItem1);
        SoldItem soldItem2 = new SoldItem(stockItem, quantity2);
        shoppingCart.addItem(soldItem2);

        Assert.assertEquals((int) shoppingCart.getAll().get(0).getQuantity(), quantity1+quantity2);
    }


    // check that the new item is added to the shopping cart
    @Test
    public void testAddingNewItem() {
        int quantity = 5;

        StockItem stockItem = new StockItem((long) 10, "Milk", "", 0.49, 20);
        dao.saveStockItem(stockItem);

        SoldItem soldItem = new SoldItem(stockItem, quantity);
        shoppingCart.addItem(soldItem);

        Assert.assertEquals(shoppingCart.getAll().get(0), soldItem);
    }


    // check that an exception is thrown if trying to add item with negative quantity
    @Test(expected = IllegalArgumentException.class)
    public void testAddingItemWithNegativeQuantity() {
        int quantity = -5;

        StockItem stockItem = new StockItem((long) 10, "Milk", "", 0.49, 20);
        dao.saveStockItem(stockItem);

        SoldItem soldItem = new SoldItem(stockItem, quantity);
        shoppingCart.addItem(soldItem);
    }


    // check that an exception is thrown if the
    // quantity of the added item is larger than quantity in warehouse
    @Test(expected = IllegalArgumentException.class)
    public void testAddingItemWithQuantityTooLarge() {
        int stock = 50;
        int buying = 5000;

        StockItem stockItem = new StockItem((long) 10, "Milk", "", 0.49, stock);
        dao.saveStockItem(stockItem);

        SoldItem soldItem = new SoldItem(stockItem, buying);
        shoppingCart.addItem(soldItem);
    }


    // check that an exception is thrown if the sum of the quantity of the added item
    // and the quantity already in shopping cart is larger than quantity in warehouse
    @Test(expected = IllegalArgumentException.class)
    public void testAddingItemWithQuantitySumTooLarge() {
        int stock = 6;
        int quantity1 = 5;
        int quantity2 = 9;

        StockItem stockItem = new StockItem((long) 10, "Milk", "", 0.49, stock);
        dao.saveStockItem(stockItem);

        SoldItem soldItem1 = new SoldItem(stockItem, quantity1);
        shoppingCart.addItem(soldItem1);
        SoldItem soldItem2 = new SoldItem(stockItem, quantity2);
        shoppingCart.addItem(soldItem2);
    }
}
