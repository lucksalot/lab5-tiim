package ee.ut.math.tvt.salessystem.logic;

import ee.ut.math.tvt.salessystem.dao.InMemorySalesSystemDAO;
import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import ee.ut.math.tvt.salessystem.logic.ShoppingCart;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InOrder;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import static org.mockito.Mockito.*;

public class ShoppingCartPurchaseTest {
    private ShoppingCart shoppingCart;
    private SalesSystemDAO dao;

    @Before
    public void setUp() throws Exception {
        dao = new InMemorySalesSystemDAO();
        shoppingCart = new ShoppingCart(dao);
    }

    // check that submitting the current purchase decreases the quantity of all StockItems
    @Test
    public void testSubmittingCurrentPurchaseDecreasesStockItemQuantity() {
        int item1Quantity = 20;
        int item2Quantity = 10;
        int item3Quantity = 5;
        StockItem stockItem1 = new StockItem((long) 10, "Milk", "", 0.49, item1Quantity);
        StockItem stockItem2 = new StockItem((long) 11, "Beer", "", 0.99, item2Quantity);
        StockItem stockItem3 = new StockItem((long) 12, "Orange", "", 0.3, item3Quantity);
        dao.saveStockItem(stockItem1);
        dao.saveStockItem(stockItem2);
        dao.saveStockItem(stockItem3);
        int quantity1 = 2;
        int quantity2 = 6;
        SoldItem soldItem1 = new SoldItem(stockItem1, quantity1);
        SoldItem soldItem2 = new SoldItem(stockItem2, quantity2);
        shoppingCart.addItem(soldItem1);
        shoppingCart.addItem(soldItem2);
        shoppingCart.submitCurrentPurchase();
        Assert.assertEquals(dao.findStockItem(soldItem1.getId()).getQuantity(), item1Quantity-quantity1);
        Assert.assertEquals(dao.findStockItem(soldItem2.getId()).getQuantity(), item2Quantity-quantity2);
        Assert.assertEquals(dao.findStockItem(stockItem3.getId()).getQuantity(), item3Quantity);
    }


    // check that submitting the current purchase calls beginTransaction and endTransaction
    // exactly once and in that order
    @Test
    public void testSubmittingCurrentPurchaseBeginsAndCommitsTransaction() {
        dao = mock(InMemorySalesSystemDAO.class);
        shoppingCart = new ShoppingCart(dao);
        shoppingCart.submitCurrentPurchase();
        verify(dao, times(1)).beginTransaction();
        verify(dao, times(1)).commitTransaction();
        InOrder order = inOrder(dao);
        order.verify(dao).beginTransaction();
        order.verify(dao).commitTransaction();
    }


    // check that a new HistoryItem is saved and that it contains the correct SoldItems
    @Test
    public void testSubmittingCurrentOrderCreatesHistoryItem() {
        ArrayList<SoldItem> soldItems = new ArrayList<>();
        StockItem stockItem1 = new StockItem((long) 10, "Milk", "", 0.49, 10);
        SoldItem soldItem1 = new SoldItem(stockItem1, 2);
        soldItems.add(soldItem1);
        shoppingCart.addItem(soldItem1);
        StockItem stockItem2 = new StockItem((long) 11, "Beer", "", 0.89, 10);
        SoldItem soldItem2 = new SoldItem(stockItem2, 3);
        soldItems.add(soldItem2);
        shoppingCart.addItem(soldItem2);

        shoppingCart.submitCurrentPurchase();
        Assert.assertTrue(dao.findAllPurchases().get(0).getSoldItemList().equals(soldItems));
    }


    // check that the timestamp on the created HistoryItem is set correctly
    // (for example has only a small difference to the current time)
    @Test
    public void testSubmittingCurrentOrderSavesCorrectTime() {
        StockItem stockItem1 = new StockItem((long) 10, "Milk", "", 0.49, 10);
        SoldItem soldItem1 = new SoldItem(stockItem1, 2);
        shoppingCart.addItem(soldItem1);
        shoppingCart.submitCurrentPurchase();
        Date date1 = new Date();
        Date date2 = dao.findAllPurchases().get(0).getDateInDate();

        SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMdd");
        Assert.assertEquals(fmt.format(date1), fmt.format(date2)); //date is the same
        Assert.assertTrue(date2.getTime() - date1.getTime() < 100);  //checks if the time is almost the same
    }


    // check that cancelling an order (with some items) and then submitting a new order
    // (with some different items) only saves the items from the new order (cancelled items are discarded)
    @Test
    public void testCancellingOrder() {
        int item1quantity = 20;
        StockItem stockItem1 = new StockItem((long) 10, "Milk", "", 0.49, item1quantity);
        dao.saveStockItem(stockItem1);
        SoldItem soldItem1 = new SoldItem(stockItem1, 2);
        shoppingCart.addItem(soldItem1);
        shoppingCart.cancelCurrentPurchase();
        int purchaseItemCount = 5;
        SoldItem soldItem2 = new SoldItem(stockItem1, purchaseItemCount);
        shoppingCart.addItem(soldItem2);
        shoppingCart.submitCurrentPurchase();
        Assert.assertEquals(dao.findStockItem((long) 10).getQuantity(), item1quantity - purchaseItemCount);
    }



    // check that after cancelling an order the quantities of the related StockItems are not changed
    @Test
    public void testCancellingOrderQuanititesUnchanged() {
        int item1quantity = 20;
        StockItem stockItem1 = new StockItem((long) 10, "Milk", "", 0.49, item1quantity);
        dao.saveStockItem(stockItem1);
        SoldItem soldItem1 = new SoldItem(stockItem1, 2);
        shoppingCart.addItem(soldItem1);
        shoppingCart.cancelCurrentPurchase();
        Assert.assertEquals(dao.findStockItem((long) 10).getQuantity(), item1quantity);
    }
}
