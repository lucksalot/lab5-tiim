package ee.ut.math.tvt.salessystem.logic;

import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.Purchase;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

public class ShoppingCart {

    private static final Logger log = LogManager.getLogger(ShoppingCart.class);

    private final SalesSystemDAO dao;
    private final List<SoldItem> items = new ArrayList<>();

    public ShoppingCart(SalesSystemDAO dao) {
        this.dao = dao;
    }

    /**
     * Add new SoldItem to table.
     */
    public void addItem(SoldItem item) {
        if (checkProductQuantity(item.getQuantity(), item.getStockItem())) {
            // Check if item already exists in the cart, update quantity if possible.
            for (SoldItem existing : items) {
                if (existing.getName().equals(item.getName())) {
                    existing.setQuantity(item.getQuantity() + existing.getQuantity());
                    log.debug("Added additional " + item.getQuantity() + item.getName() + " to the cart.");
                    return;
                }
            }
            items.add(item);
            log.debug("Cart: Added " + item.getName() + " quantity of " + item.getQuantity());
        }
    }

    public List<SoldItem> getAll() {
        return items;
    }

    public void cancelCurrentPurchase() {
        items.clear();
    }

    public void submitCurrentPurchase() {
        // TODO decrease quantities of the warehouse stock

        // note the use of transactions. InMemorySalesSystemDAO ignores transactions
        // but when you start using hibernate in lab5, then it will become relevant.
        // what is a transaction? https://stackoverflow.com/q/974596

        try {
            dao.beginTransaction();
            Purchase purchase = new Purchase(items);
            for (SoldItem item : items) {
                dao.saveSoldItem(item);
                long barCode = item.getId();
                int quantity = item.getQuantity();
                dao.findStockItems().stream().filter(Sold -> barCode == Sold.getId()).forEach(Sold -> {
                            Sold.setQuantity(Sold.getQuantity() - quantity);
                });
                item.setPurchase(purchase);
            }
            dao.savePurchase(purchase);
            items.clear();
            dao.commitTransaction();
        } catch (Exception e) {
            dao.rollbackTransaction();
            throw e;
        }
    }

    /**
     * Checks how many items are available in the warehouse vs attempted to purchase.
     * Returns false if trying to buy more than available.
     */
    private boolean checkProductQuantity(int quantity, StockItem stockItem) {

        if(quantity > stockItem.getQuantity() || quantity <= 0) {
            log.error("Exception: Attempted to add " + quantity + " items to cart, only "
                    + stockItem.getQuantity() + " available.");
            throw new IllegalArgumentException();
        }
        return true;
    }
}
