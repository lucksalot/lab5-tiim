package ee.ut.math.tvt.salessystem.dao;

import ee.ut.math.tvt.salessystem.dataobjects.Purchase;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;

import java.util.ArrayList;
import java.util.List;

public class InMemorySalesSystemDAO implements SalesSystemDAO {

    private final List<StockItem> stockItemList;
    private final List<SoldItem> soldItemList;
    private final List<Purchase> purchaseList;
    private Boolean isTransactionOn = false;



    public InMemorySalesSystemDAO() {
        List<StockItem> items = new ArrayList<StockItem>();
//        items.add(new StockItem(1L, "Lays chips", "Potato chips", 11.0, 5));
//        items.add(new StockItem(2L, "Chupa-chups", "Sweets", 8.0, 8));
//        items.add(new StockItem(3L, "Frankfurters", "Beer sauseges", 15.0, 12));
//        items.add(new StockItem(4L, "Free Beer", "Student's delight", 0.0, 100));
        this.stockItemList = items;
        this.soldItemList = new ArrayList<>();
        purchaseList = new ArrayList<>();
    }

    @Override
    public List<StockItem> findStockItems() {
        return stockItemList;
    }

    @Override
    public List<SoldItem> findSoldItems() {
        return soldItemList;
    }

    @Override
    public List<Purchase> findAllPurchases(){
        return purchaseList;
    }


    @Override
    public StockItem findStockItem(long id) {
        for (StockItem item : stockItemList) {
            if (item.getId() == id)
                return item;
        }
        return null;
    }

    @Override
    public StockItem findStockItem(String name) {
        for (StockItem item : stockItemList) {
            if (item.getName().equals(name))
                return item;
        }
        return null;
    }

    @Override
    public void saveSoldItem(SoldItem item) {
        beginTransaction();
        while(isTransactionOn){
            if (item.getQuantity()>0 && item.getPrice()>=0.0) {
                soldItemList.add(item);
                commitTransaction();
            }else{
                rollbackTransaction();
                System.out.println("Mittesobiv sisend!");
            }
        }


    }

    @Override
    public void saveStockItem(StockItem stockItem) {
        stockItemList.add(stockItem);
    }

    @Override
    public void savePurchase(Purchase purchase) {
        purchaseList.add(purchase);
    }

    @Override
    public void beginTransaction() {
        isTransactionOn = true;
    }

    @Override
    public void rollbackTransaction() {
        isTransactionOn = false;
    }

    @Override
    public void commitTransaction() {
        isTransactionOn = false;
        soldItemList.clear();
    }

    @Override
    public void updateStock(StockItem stockItem, int quantity) {

    }

    public Boolean getTransactionOn() {
        return isTransactionOn;
    }
}
