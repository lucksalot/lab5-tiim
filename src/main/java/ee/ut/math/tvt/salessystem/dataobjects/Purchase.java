package ee.ut.math.tvt.salessystem.dataobjects;


import javax.persistence.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "PURCHASE")
public class Purchase {

    //private final String date;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "DATE")
    private String date;

    @Column(name = "TIME")
    private String time;

    @Column(name = "TOTAL")
    private double total;


    private final List<SoldItem> soldItemList;

    private Date dateInDate;

    public Purchase() {
        soldItemList = new ArrayList<>();
    }

    public Purchase(List<SoldItem> soldItemList){
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date currentDate = new Date();
        this.dateInDate = currentDate;
        String[] dateData = dateFormat.format(currentDate).split(" ");
        this.date = dateData[0];
        this.time = dateData[1];
        this.total = 0;
        this.soldItemList = new ArrayList<>(soldItemList);
        for (SoldItem solditem: soldItemList) {
            total += solditem.getSum();
        }
    }


    public long getId() {
        return this.id;
    }

    /*public List<SoldItem> getSoldItemList() {
        return soldItemList;
    }*/

    public Date getDateInDate(){
        return dateInDate;
    }
    public String getDate() {
        return date;
    }

    public String getTime() {
        return time;
    }

    public String toString() {
        return date + "  " + time;
    }


    public void setDate(String date) {
        this.date = date;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public List<SoldItem> getSoldItemList() {
        return this.soldItemList;
    }
}
