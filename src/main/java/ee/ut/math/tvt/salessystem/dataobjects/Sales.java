package ee.ut.math.tvt.salessystem.dataobjects;

import javax.persistence.*;


@Entity
@Table(name = "SALES")
public class Sales {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne
    @JoinColumn(name = "PURCHASE_ID",nullable = false)
    private Purchase purchaseID;

    //@ManyToMany
    //@JoinColumn(name = "SOLDITEM_ID", nullable = false)
    //private SoldItem soldItemID;

    @Column(name = "PRICE")
    private Double price;


    @Column(name = "QUANTITY")
    private Integer Quantity;
}
