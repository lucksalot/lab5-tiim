package ee.ut.math.tvt.salessystem.dao;

import ee.ut.math.tvt.salessystem.dataobjects.Purchase;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.List;

public class HibernateSalesSystemDAO implements SalesSystemDAO {

    private final EntityManagerFactory emf;
    private final EntityManager em;

    public HibernateSalesSystemDAO() {
        // if you get ConnectException/JDBCConnectionException then you
        // probably forgot to start the database before starting the application
        emf = Persistence.createEntityManagerFactory("pos");
        em = emf.createEntityManager();
    }

    public void close() {
        em.close();
        emf.close();
    }

    @Override
    public List<SoldItem> findSoldItems() {
        return em.createQuery("select s from SoldItem s", SoldItem.class).getResultList();
    }


    public List<StockItem> findStockItems() {
        return em.createQuery("select s from StockItem s", StockItem.class).getResultList();
    }

    @Override
    public List<Purchase> findAllPurchases() {
        return em.createQuery("select p from Purchase p", Purchase.class).getResultList();
    }

    @Override
    public StockItem findStockItem(long id) {
        List<StockItem> items = em.createQuery("select s from StockItem s where s.id = :id", StockItem.class)
                .setParameter( "id", id)
                .getResultList();
        if(items.size() == 0) {
            return null;
        } else {
            return items.get(0);
        }
    }

    @Override
    public StockItem findStockItem(String name) {
        List<StockItem> items = em.createQuery("select s from StockItem s where s.name = :name", StockItem.class)
                .setParameter( "name", name)
                .getResultList();
        if(items.size() == 0) {
            return null;
        } else {
            return items.get(0);
        }
    }

    @Override
    public void saveSoldItem(SoldItem soldItem) {
        beginTransaction();
        try {
            em.persist(soldItem);
            commitTransaction();
        }catch (Exception e){
            rollbackTransaction();
        }
    }

    @Override
    public void saveStockItem(StockItem stockItem) {
        beginTransaction();
        try {
            em.persist(stockItem);
            commitTransaction();
        }catch (Exception e){
            rollbackTransaction();
        }
    }

    @Override
    public void savePurchase(Purchase purchase) {
        beginTransaction();
        try {
            em.persist(purchase);
            commitTransaction();
        }catch (Exception e){
            rollbackTransaction();
        }
    }


    @Override
    public void beginTransaction() {
        em.getTransaction().begin();
    }

    @Override
    public void rollbackTransaction() {
        em.getTransaction().rollback();
    }

    @Override
    public void commitTransaction() {
        em.getTransaction().commit();
    }

    public void updateStock(StockItem stockItem, int quantity) {
        beginTransaction();
        try {
            int updatedEntities = em.createQuery(
                            "update StockItem s " +
                            "set s.quantity = :newQuantity " +
                            "where s.id = :id")
                    .setParameter("newQuantity", stockItem.getQuantity() + quantity)
                    .setParameter("id", stockItem.getId())
                    .executeUpdate();
            stockItem.setQuantity(stockItem.getQuantity() + quantity);
            commitTransaction();
        } catch (Exception e) {
            rollbackTransaction();
        }
    }
}