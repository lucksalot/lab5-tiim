package ee.ut.math.tvt.salessystem.logic;

import ee.ut.math.tvt.salessystem.dao.InMemorySalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;

import java.util.ArrayList;
import java.util.List;

public class Warehouse {
    private InMemorySalesSystemDAO dao;
    private Boolean wasTransactionStarted = false;
    private Boolean wasTransactionFinished = false;
    private List<StockItem> items;

    public Warehouse() {
        dao = new InMemorySalesSystemDAO();
        items = new ArrayList<>();
    }

    public List<StockItem> getItems() {
        return items;
    }

    public void addItem(StockItem stockItem) {
        //Cases : 1. new item 2. in stock item 3. invalid value ( negative )

        //check if StockItem is valid
        if (stockItem.getQuantity() > 0 && stockItem.getPrice() >= 0.0) {

            if (items.contains(stockItem)) {

                //find the stockitem and add it's quantity
                StockItem containedItem = dao.findStockItem(stockItem.getId());

                if (containedItem.getName().equals(stockItem.getName()) &&
                        containedItem.getDescription().equals((stockItem.getDescription()))) {
                    addExistingItem(stockItem);
                }
                else throw new IllegalArgumentException();

            } else {
                //otherwise add the item
                if (stockItem.getId() > 0L) addNewItem(stockItem);
                else {
                    dao.rollbackTransaction();
                    throw new IllegalArgumentException();
                }
            }

        }
        else throw new IllegalArgumentException();
    }

    private void addNewItem(StockItem stockItem) {
        items.add(stockItem);
        refreshItems(stockItem);
    }

    private void addExistingItem(StockItem stockItem) {
        refreshItems(stockItem);
    }

    private void refreshItems(StockItem stockItem) {
        dao.beginTransaction();
        if (dao.getTransactionOn()) wasTransactionStarted = true;
        try {
            for (StockItem item : items) {
                if (!dao.findStockItems().contains(stockItem)) dao.saveStockItem(item);
                else
                    dao.findStockItem(stockItem.getId()).setQuantity(dao.findStockItem(stockItem.getId()).getQuantity() + item.getQuantity());
            }
            dao.commitTransaction();
            if (!dao.getTransactionOn()) wasTransactionFinished = true;
        } catch (Exception e) {
            dao.rollbackTransaction();
        }
    }


    public Boolean getWasTransactionStarted() {
        return wasTransactionStarted;
    }

    public Boolean getWasTransactionFinished() {
        return wasTransactionFinished;
    }


    public StockItem findItem(Long id) {
        for (StockItem item : items) {
            if (item.getId() == id) return item;
        }
        return null;
    }
}




