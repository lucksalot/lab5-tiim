package ee.ut.math.tvt.salessystem.ui;

import ee.ut.math.tvt.salessystem.SalesSystemException;
import ee.ut.math.tvt.salessystem.dao.InMemorySalesSystemDAO;
import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.Purchase;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import ee.ut.math.tvt.salessystem.logic.ShoppingCart;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Properties;

/**
 * A simple CLI (limited functionality).
 */
public class ConsoleUI {
    private static final Logger log = LogManager.getLogger(ConsoleUI.class);

    private final SalesSystemDAO dao;
    private final ShoppingCart cart;

    public ConsoleUI(SalesSystemDAO dao) {
        this.dao = dao;
        cart = new ShoppingCart(dao);
    }

    public static void main(String[] args) throws Exception {
        SalesSystemDAO dao = new InMemorySalesSystemDAO();
        ConsoleUI console = new ConsoleUI(dao);
        console.run();
    }

    /**
     * Run the sales system CLI.
     */
    public void run() throws IOException {
        System.out.println("===========================");
        System.out.println("=       Sales System      =");
        System.out.println("===========================");
        printUsage();
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        while (true) {
            System.out.print("> ");
            processCommand(in.readLine().trim().toLowerCase());
            System.out.println("Done. ");
        }
    }

    private void displayTeam() {

        Properties properties = new Properties();
        Path propFile = Paths.get("..//src//main//resources//application.properties");

        try {
            properties.load(Files.newBufferedReader(propFile));
            System.out.println("Team name: "+properties.getProperty("team_name"));
            System.out.println("Team leader: "+properties.getProperty("team_leader"));
            System.out.println("Team leader's email: "+properties.getProperty("team_leader_email"));
            System.out.println("Team member 1: "+properties.getProperty("team_member1"));
            System.out.println("Team member 2: "+properties.getProperty("team_member2"));
            System.out.println("Team member 3: "+properties.getProperty("team_member3"));
        }
        catch (IOException e) {
            System.out.println("Properties file could not be found. Details: " + e);
        }
    }

    private void showStock() {
        List<StockItem> stockItems = dao.findStockItems();
        System.out.println("-------------------------");
        for (StockItem si : stockItems) {
            System.out.println(si.getId() + " " + si.getName() + " " + si.getPrice() + "Euro (" + si.getQuantity() + " items)");
        }
        if (stockItems.size() == 0) {
            System.out.println("\tNothing");
        }
        System.out.println("-------------------------");
    }

    private void showCart() {
        System.out.println("-------------------------");
        for (SoldItem si : cart.getAll()) {
            System.out.println(si.getName() + " " + si.getPrice() + "Euro (" + si.getQuantity() + " items)");
        }
        if (cart.getAll().size() == 0) {
            System.out.println("\tNothing");
        }
        System.out.println("-------------------------");
    }

    private void pastPurchases(int number) {
        for (Purchase purchase : dao.findAllPurchases()) {
            System.out.println(purchase.getDate() + "\t" + purchase.getTime());
            System.out.println("ID" + "\t" + "Name" + "\t\t" + "Price" + "\t" +
                    "Amount" + "\t" + "Sum");
            for (SoldItem item : purchase.getSoldItemList()) {
                System.out.println(item.getId() + "\t" + item.getName() + "\t" + item.getPrice() + "\t" +
                        item.getQuantity() + "\t" + item.getSum());
            }
            number--;
            if(number == 0) break;
        }
    }

    private void printUsage() {
        System.out.println("------------------------------------");
        System.out.println("Usage:");
        System.out.println("h\t\t\tShow this help");
        System.out.println("w\t\t\tShow warehouse contents");
        System.out.println("n IDX QTY [$ NAME]\tAdd item to warehouse");
        System.out.println("c\t\t\tShow cart contents");
        System.out.println("a IDX NR \t\tAdd NR of stock item with index IDX to the cart");
        System.out.println("p\t\t\tPurchase the shopping cart");
        System.out.println("r\t\t\tReset the shopping cart");
        System.out.println("b NR\t\t\tShow NR of previous purchases");
        System.out.println("t\t\t\tShow team information");
        System.out.println("------------------------------------");
    }

    private void processCommand(String command) {
        String[] c = command.split(" ");

        if (c[0].equals("h"))
            printUsage();
        else if (c[0].equals("t"))
            displayTeam();
        else if (c[0].equals("q"))
            System.exit(0);
        else if (c[0].equals("w"))
            showStock();
        else if (c[0].equals("c"))
            showCart();
        else if (c[0].equals("p"))
            cart.submitCurrentPurchase();
        else if (c[0].equals("r")) {
            for (SoldItem solditem : cart.getAll()) {
                StockItem stockitem = dao.findStockItem(solditem.getId());
                stockitem.setQuantity(stockitem.getQuantity() + solditem.getQuantity());
            }
            cart.cancelCurrentPurchase();
        }

        else if (c[0].equals("a") && c.length == 3) {
            try {
                long idx = Long.parseLong(c[1]);
                int amount = Integer.parseInt(c[2]);
                StockItem item = dao.findStockItem(idx);
                if (item != null) {
                    int add =  Math.min(amount, item.getQuantity());
                    cart.addItem(new SoldItem(item, add));
                    item.setQuantity(item.getQuantity() - add);
                    System.out.println(add + " items added to cart.");
                } else {
                    System.out.println("no stock item with id " + idx);
                }
            } catch (SalesSystemException | NoSuchElementException e) {
                log.error(e.getMessage(), e);
            }
        }

        else if (c[0].equals("b") && c.length == 2) {
            try {
                int number = Integer.parseInt(c[1]);
                pastPurchases(number);
            } catch (SalesSystemException e) {
                log.error(e.getMessage(), e);
            }
        }

        else if (c[0].equals("n") && c.length >= 3) {
            try {
                long idx = Long.parseLong(c[1]);
                int amount = Integer.parseInt(c[2]);

                StockItem stockItem = dao.findStockItem(idx);

                if (stockItem != null) {
                    stockItem.setQuantity(stockItem.getQuantity() + amount);

                    System.out.println("Item quantity updated: "
                            + stockItem.getName()
                            + ", new stock: "
                            + stockItem.getQuantity());
                } else if (c.length >= 5) {

                    StringBuilder sb = new StringBuilder();
                    for (int i = 4; i < c.length ; i++) {
                        sb.append(c[i]);
                        sb.append(" ");
                    }
                    sb.setLength(Math.max(sb.length() - 1, 0));
                    String name = sb.toString();

                    double price = Double.parseDouble(c[3]);
                    dao.saveStockItem(new StockItem(idx, name, "", price, amount));

                    System.out.println("New item added to warehouse: "
                            + amount + " items of "
                            + name + " with the price "
                            + price + ".");
                } else {
                    System.out.println("Not enough information given.");
                }

            } catch (SalesSystemException e) {
                log.error(e.getMessage(), e);
            }
        }

        else {
            System.out.println("unknown command");
        }
    }

}
